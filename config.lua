-- Board scale
SCALE = 0.412

-- Cards scales
SCALE_WIDTH = 0.42
SCALE_HEIGHT = 0.32

-- Zones aliases
MELEE = 1
DISTANCE = 2
SIEGE = 3
WEATHER = 4
BOOST = 5

-- To be changed
BOARD = 1

-- Colors
WHITE = {255,255,255}
ORANGE = {232,126,4}

-- Network client/server variable and tools
IS_SERVER = false
YOUR_TURN = false
IP = ""

function addLeadingZero(number)
  if tonumber(number) < 10 then
    return "0" .. number
  end
  return number
end

-- Pass hand
PASS = false
AD_PASS = false
angle = 0
WIN_ROUND = false
AD_WIN_ROUND = false
IS_WINNER = false

-- GUI Buttons positions
local WIDTH, HEIGHT = love.graphics.getDimensions( )
X_POS = WIDTH / 2 - 150
Y_CENTER = HEIGHT / 2 - 15
