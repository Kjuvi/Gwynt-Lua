require "../config"

local menu = {}

local suit = require "../lib/suit"
local timer
local background


function menu:init()
  timer = 0
  background = {
    love.graphics.newImage("assets/home.jpg"),
    love.graphics.newImage("assets/home2.jpg"),
    love.graphics.newImage("assets/home3.jpg"),
    love.graphics.newImage("assets/home4.png")
    }
    backgroundDrawn=background[love.math.random(#background)]
      

  suit.theme.color.normal = {fg = {0,0,0}, bg = {236, 240, 241, 106}}
  suit.theme.color.hovered = {fg = {0,0,0}, bg = {236, 240, 241, 136}}
  suit.theme.color.active = {fg = {255,255,255}, bg = {0,0,0,136}}
end


function menu:draw()
  love.graphics.draw(backgroundDrawn, 0, 0, 0, 1, 1, 0, 0, 0, 0)
  suit.draw()
end


function menu:update(dt)
  timer = timer + dt
  
  if timer > 0.15 and suit.Button("Play", X_POS, Y_CENTER + 25, 300,30).hit then
    gamestate.switch(connect)
  end
  
  if timer > 0.3 and suit.Button("Settings", X_POS, Y_CENTER + 65, 300, 30).hit then
    gamestate.switch(settings)
  end
  
  if timer > 0.45 and suit.Button("Quit", X_POS, Y_CENTER + 105, 300, 30).hit then
    love.event.quit()
  end
end


return menu
