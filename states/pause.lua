require "../config"

local pause = {}
local suit = require "../lib/suit"

function pause:enter(game)
  gui = suit.new()
end


function pause:draw()
  gui:draw()
end


function pause:update(dt)
  local WIDTH, HEIGHT = love.graphics.getDimensions( )
  X_POS = WIDTH / 2 - 150
  Y_CENTER = HEIGHT / 2 - 15

  if gui:Button("Resume", X_POS, Y_CENTER - 35, 300,30).hit then
    gamestate.switch(game)    
  end

  if gui:Button("Quit", X_POS, Y_CENTER + 5, 300, 30).hit then
    love.event.quit()
  end
end


function pause:keypressed(key, scancode, isrepeat)
  if key == 'escape' then
    gamestate.switch(game)
  end
end


return pause
