local waitScreen = {}
local suit = require "../lib/suit"


function waitScreen:init()
  love.graphics.setBackgroundColor(255,255,255)
  timer = 0
  if IS_SERVER then 
    text = "The server is now waiting for a connection..."
  else
    text = "The client will now attempt to connect to a server..."
  end
end


function waitScreen:draw()
  suit.draw()
end


function waitScreen:update(dt)
  timer = timer +dt
  suit.Label(text, X_POS,Y_CENTER,300,30)

  if timer > 2 then
    gamestate.switch(game)
  end
end 

return waitScreen  
