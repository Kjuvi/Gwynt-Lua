local win = {}
local suit = require "../lib/suit"

function win:init()
  love.window.setMode(800,450)
  love.graphics.setBackgroundColor(255,255,255)

  if IS_WINNER then 
    image = love.graphics.newImage("assets/victory.PNG")
  else
    image = love.graphics.newImage("assets/defeat.PNG")
  end

  suit.theme.color.normal = {fg = {0,0,0}, bg = {236, 240, 241, 106}}
  suit.theme.color.hovered = {fg = {0,0,0}, bg = {236, 240, 241, 136}}
  suit.theme.color.active = {fg = {255,255,255}, bg = {0,0,0,136}}
end


function win:draw()
  love.graphics.draw(image, 0, 0, 0, 1, 1, 0, 0, 0, 0)
  suit.draw()
end


function win:update(dt)
  if suit.Button("Quit", X_POS, 400, 300,30).hit then
    love.event.quit()
  end
end

return win
