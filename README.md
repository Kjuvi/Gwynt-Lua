# Gwynt-Lua
Haute école ARC's summer project. The game is based on the Gwynt game of CD Red Projekt. It's entirely scripted in Lua with the Love2D Framework.


# Libraries

1. [Love2D](https://love2d.org/)
2. [Lua-Enet](http://leafo.net/lua-enet/)
3. [hump](https://github.com/vrld/hump)
4. [SUIT](https://github.com/vrld/SUIT)
5. [Simple Tiled Implementation](https://github.com/karai17/Simple-Tiled-Implementation)
