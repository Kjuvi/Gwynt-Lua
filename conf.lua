function love.conf(t)
  t.window.width = 800
  t.window.height = 450
  t.window.title = "Gwynt"
  
  t.author  = "Dany Chea & Quentin Vaucher"
  t.url = "https://github.com/QVaucher/Gwynt-Lua"
end