local placeholders = {}

function placeholders:hand()
  return board.layers["HAND"].objects
end


function placeholders:zones()
  return {
    board.layers["MELEE"].objects,
    board.layers["DISTANCE"].objects,
    board.layers["SIEGE"].objects,
    board.layers["WEATHER"].objects,
    board.layers["BOOST"].objects
    --board.layers["BOARD"].objects
  }
end


function placeholders:adZones()
  return {
    board.layers["MELEE_AD"].objects,
    board.layers["DISTANCE_AD"].objects,
    board.layers["SIEGE_AD"].objects,
    board.layers["WEATHER"].objects,
    board.layers["BOOST_AD"].objects
  }
end

function placeholders:sound()
  return board.layers["SOUND"].objects
end

function placeholders:peripheralZones()
  return board.layers["ZONES"].objects
end

function placeholders:zonesScores()
  return board.layers["ZONES_SCORES"].objects
end

function placeholders:adZonesScores()
  return board.layers["ZONES_SCORES_AD"].objects
end

function placeholders:score()
  return board.layers["SCORE"].objects
end

function placeholders:help()
  return board.layers["HELP"].objects
end

return placeholders