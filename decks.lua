local card = require "card"

function applyWeatherConditions(zone, zoneScore)
  zoneScore = 0

  for i=1, #placeholders:zones(), 1 do
    if zone[i] ~= nil then
      zoneScore = zoneScore + 1
    end
  end

  return zoneScore
end


local capacity = {
  bitingFrost = function()
    contents.zonesScores[1] = applyWeatherConditions(contents.zones[1], contents.zonesScores[1])
    contents.score[1] = contents.zonesScores[1] + contents.zonesScores[2] + contents.zonesScores[3]

    contents.adZonesScores[1] = applyWeatherConditions(contents.adZones[1], contents.adZonesScores[1])
    contents.adScore[1] = contents.adZonesScores[1] + contents.adZonesScores[2] + contents.adZonesScores[3]
  end,

  impenetrableFog = function()
    contents.zonesScores[2] = applyWeatherConditions(contents.zones[2], contents.zonesScores[2])
    contents.score[1] = contents.zonesScores[1] + contents.zonesScores[2] + contents.zonesScores[3]

    contents.adZonesScores[2] = applyWeatherConditions(contents.adZones[2], contents.adZonesScores[2])
    contents.adScore[1] = contents.adZonesScores[1] + contents.adZonesScores[2] + contents.adZonesScores[3]
  end,

  torrentialRain = function()
    contents.zonesScores[3] = applyWeatherConditions(contents.zones[3], contents.zonesScores[3])
    contents.score[1] = contents.zonesScores[1] + contents.zonesScores[2] + contents.zonesScores[3]

    contents.adZonesScores[3] = applyWeatherConditions(contents.adZones[3], contents.adZonesScores[3])
    contents.adScore[1] = contents.adZonesScores[1] + contents.adZonesScores[2] + contents.adZonesScores[3]
  end,

  clearWeather = function()
    scores:updateZonesScores()
    scores:updateAdZonesScores()
  end
}


leaderImg={
  --**************WILDHUNT*****************************
  {
    love.graphics.newImage("assets/The Wild Hunt/Leader/134.png"),
    love.graphics.newImage("assets/The Wild Hunt/Leader/135.png"),
    love.graphics.newImage("assets/The Wild Hunt/Leader/136.png"),
    love.graphics.newImage("assets/The Wild Hunt/Leader/137.png")
  },
  --***********SCOIATAEL*************************************
  {
    love.graphics.newImage("assets/Scoia'tael/Leader/13.png"),
    love.graphics.newImage("assets/Scoia'tael/Leader/14.png"),
    love.graphics.newImage("assets/Scoia'tael/Leader/15.png"),
    love.graphics.newImage("assets/Scoia'tael/Leader/16.png")
  },

--*****************NILFGAARD*********************************
  {
    love.graphics.newImage("assets/Nilfgaard/Leader/01.png"),
    love.graphics.newImage("assets/Nilfgaard/Leader/02.png"),
    love.graphics.newImage("assets/Nilfgaard/Leader/03.png"),
    love.graphics.newImage("assets/Nilfgaard/Leader/04.png")
  },
  --**************NORTHERN REALMS****************************
  {
    love.graphics.newImage("assets/Northern Realms/Leader/05.png"),
    love.graphics.newImage("assets/Northern Realms/Leader/06.png"),
    love.graphics.newImage("assets/Northern Realms/Leader/07.png"),
    love.graphics.newImage("assets/Northern Realms/Leader/08.png")
  }
}


factionsDeck = {
--**********************************************
--*                   WILD HUNT                *
--**********************************************
  {
    --melee
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/013.png"), deckIndex = 1},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/016.png"), deckIndex = 2},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/018.png"), deckIndex = 3},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/019.png"), deckIndex = 4},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/020.png"), deckIndex = 5},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/021.png"), deckIndex = 6},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/022.png"), deckIndex = 7},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/023.png"), deckIndex = 8},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/026.png"), deckIndex = 9},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/027.png"), deckIndex = 10},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/028.png"), deckIndex = 11},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/029.png"), deckIndex = 12},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/030.png"), deckIndex = 13},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/032.png"), deckIndex = 14},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/033.png"), deckIndex = 15},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/035.png"), deckIndex = 16}, -- this card can be both zone 1 and 2
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/039.png"), deckIndex = 17},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/041.png"), deckIndex = 18},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/042.png"), deckIndex = 19},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/043.png"), deckIndex = 20},
    card:new{value = 1, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/044.png"), deckIndex = 21},
    card:new{value = 1, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/047.png"), deckIndex = 22},
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/143.png"), deckIndex = 23},
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/The Wild Hunt/144.png"), deckIndex = 24},

    --distance
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/The Wild Hunt/025.png"), deckIndex = 25},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/The Wild Hunt/034.png"), deckIndex = 26},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/The Wild Hunt/036.png"), deckIndex = 27},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/The Wild Hunt/037.png"), deckIndex = 28},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/The Wild Hunt/040.png"), deckIndex = 29},
    card:new{value = 10, zone = DISTANCE, image = love.graphics.newImage("assets/The Wild Hunt/145.png"), deckIndex = 30},
    card:new{value = 8, zone = DISTANCE, image = love.graphics.newImage("assets/The Wild Hunt/146.png"), deckIndex = 31}, -- this card can be both zone 1 and 2

    --siege
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/012.png"), deckIndex = 32},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/012.png"), deckIndex = 33},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/012.png"), deckIndex = 34},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/014.png"), deckIndex = 35},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/014.png"), deckIndex = 36},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/014.png"), deckIndex = 37},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/015.png"), deckIndex = 38},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/015.png"), deckIndex = 39},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/The Wild Hunt/015.png"), deckIndex = 40},

    --weather
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 41},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 42},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 43},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 44},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 45},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 46},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 47},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 48},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 49},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 50},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 51},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 52},

    --Special
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 53}, -- this card can go everywhere
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 54},
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 55},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 56}, -- this card can go everywhere
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 57},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 58},
    --incineration to be implemented

    --Unique
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/008.png"), deckIndex = 59},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Neutral/009.png"), deckIndex = 60},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/010.png"), deckIndex = 61},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/Neutral/011.png"), deckIndex = 62},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/138.png"), deckIndex = 63},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/139.png"), deckIndex = 64},
    card:new{value = 7, zone = DISTANCE, image = love.graphics.newImage("assets/Neutral/140.png"), deckIndex = 65},
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/141.png"), deckIndex = 66},
    card:new{value = 0, zone = MELEE, image = love.graphics.newImage("assets/Neutral/142.png"), deckIndex = 67},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/155.png"), deckIndex = 68}

  },

--**********************************************
--*                   SCOIA'TAEL               *
--**********************************************
  {
    --Hero melee
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/19.png"), deckIndex = 1},


    --Melee or distance
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/102.png"), deckIndex = 2},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/104.png"), deckIndex = 3},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/105.png"), deckIndex = 4},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/106.png"), deckIndex = 5},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/107.png"), deckIndex = 6},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/108.png"), deckIndex = 7},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/109.png"), deckIndex = 8},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/113.png"), deckIndex = 9},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/114.png"), deckIndex = 10},

    --melee 
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/110.png"), deckIndex = 11},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/111.png"), deckIndex = 12},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/112.png"), deckIndex = 13},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/115.png"), deckIndex = 14},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/116.png"), deckIndex = 15},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/117.png"), deckIndex = 16},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/118.png"), deckIndex = 17},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/119.png"), deckIndex = 18},
    card:new{value = 3, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/123.png"), deckIndex = 19},
    card:new{value = 3, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/124.png"), deckIndex = 20},
    card:new{value = 3, zone = MELEE, image = love.graphics.newImage("assets/Scoia'tael/125.png"), deckIndex = 21},

    --distance   
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/120.png"), deckIndex = 22},
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/121.png"), deckIndex = 23},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/126.png"), deckIndex = 24},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/127.png"), deckIndex = 25},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/128.png"), deckIndex = 26},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/129.png"), deckIndex = 27},
    card:new{value = 1, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/130.png"), deckIndex = 28},
    card:new{value = 0, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/131.png"), deckIndex = 29},
    card:new{value = 0, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/132.png"), deckIndex = 30},
    card:new{value = 0, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/133.png"), deckIndex = 31},
    card:new{value = 10, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/17.png"), deckIndex = 32},
    card:new{value = 10, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/18.png"), deckIndex = 33},
    card:new{value = 10, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/20.png"), deckIndex = 34},
    card:new{value = 1, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/129.png"), deckIndex = 35},
    card:new{value = 1, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/130.png"), deckIndex = 36},
    card:new{value = 0, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/131.png"), deckIndex = 37},
    card:new{value = 0, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/132.png"), deckIndex = 38},
    card:new{value = 0, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/133.png"), deckIndex = 39},
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Scoia'tael/120.png"), deckIndex = 40},
   
   --weather
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 41},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 42},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 43},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 44},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 45},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 46},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 47},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 48},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 49},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 50},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 51},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 52},

    --Special
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 53}, -- this card can go everywhere
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 54},
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 55},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 56}, -- this card can go everywhere
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 57},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 58},
    --incineration to be implemented

    --Unique
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/008.png"), deckIndex = 59},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Neutral/009.png"), deckIndex = 60},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/010.png"), deckIndex = 61},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/Neutral/011.png"), deckIndex = 62},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/138.png"), deckIndex = 63},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/139.png"), deckIndex = 64},
    card:new{value = 7, zone = DISTANCE, image = love.graphics.newImage("assets/Neutral/140.png"), deckIndex = 65},
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/141.png"), deckIndex = 66},
    card:new{value = 0, zone = MELEE, image = love.graphics.newImage("assets/Neutral/142.png"), deckIndex = 67},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/155.png"), deckIndex = 68}
  },
--**********************************************
--*                   NILFGAARD                *
--**********************************************
  {
    --HEROS
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/147.png"), deckIndex = 1},
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/148.png"), deckIndex = 2},
    card:new{value = 10, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/149.png"), deckIndex = 3},
    card:new{value = 10, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/150.png"), deckIndex = 4},
    
    --MELEE
    card:new{value = 9, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/050.png"), deckIndex = 5},
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/051.png"), deckIndex = 6},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/054.png"), deckIndex = 7},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/058.png"), deckIndex = 8},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/059.png"), deckIndex = 9},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/061.png"), deckIndex = 10},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/063.png"), deckIndex = 11},
    card:new{value = 3, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/064.png"), deckIndex = 12},
    card:new{value = 3, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/067.png"), deckIndex = 13},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/070.png"), deckIndex = 14},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/Nilfgaard/071.png"), deckIndex = 15},

    --DISTANCE
    card:new{value = 10, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/47.png"), deckIndex = 16},
    card:new{value = 10, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/048.png"), deckIndex = 17},
    card:new{value = 6, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/052.png"), deckIndex = 18},
    card:new{value = 6, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/053.png"), deckIndex = 19},
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/056.png"), deckIndex = 20},
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/060.png"), deckIndex = 21},
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/062.png"), deckIndex = 22},
    card:new{value = 3, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/066.png"), deckIndex = 23},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/068.png"), deckIndex = 24},
    card:new{value = 2, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/069.png"), deckIndex = 25},
    card:new{value = 1, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/072.png"), deckIndex = 26},
    card:new{value = 1, zone = DISTANCE, image = love.graphics.newImage("assets/Nilfgaard/073.png"), deckIndex = 27},
    
    --SIEGE
    card:new{value = 10, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/049.png"), deckIndex = 28},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/055.png"), deckIndex = 29},
    card:new{value = 5, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/057.png"), deckIndex = 30},
    card:new{value = 3, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/065.png"), deckIndex = 31},
    card:new{value = 0, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/074.png"), deckIndex = 32},
    card:new{value = 10, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/049.png"), deckIndex = 33},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/055.png"), deckIndex = 34},
    card:new{value = 5, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/057.png"), deckIndex = 35},
    card:new{value = 3, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/065.png"), deckIndex = 36},
    card:new{value = 0, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/074.png"), deckIndex = 37},
    card:new{value = 5, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/057.png"), deckIndex = 38},
    card:new{value = 3, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/065.png"), deckIndex = 39},
    card:new{value = 0, zone = SIEGE, image = love.graphics.newImage("assets/Nilfgaard/074.png"), deckIndex = 40},

    --weather
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 41},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 42},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 43},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 44},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 45},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 46},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 47},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 48},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 49},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 50},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 51},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 52},

    --Special
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 53}, -- this card can go everywhere
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 54},
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 55},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 56}, -- this card can go everywhere
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 57},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 58},
    --incineration to be implemented

    --Unique
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/008.png"), deckIndex = 59},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Neutral/009.png"), deckIndex = 60},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/010.png"), deckIndex = 61},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/Neutral/011.png"), deckIndex = 62},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/138.png"), deckIndex = 63},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/139.png"), deckIndex = 64},
    card:new{value = 7, zone = DISTANCE, image = love.graphics.newImage("assets/Neutral/140.png"), deckIndex = 65},
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/141.png"), deckIndex = 66},
    card:new{value = 0, zone = MELEE, image = love.graphics.newImage("assets/Neutral/142.png"), deckIndex = 67},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/155.png"), deckIndex = 68}

  },
--**********************************************
--*                NORTHERN REALMS             *
--**********************************************
  {
    --HEROES
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/09.png"), deckIndex = 1},
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/10.png"), deckIndex = 2},
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/11.png"), deckIndex = 3},
    card:new{value = 10, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/12.png"), deckIndex = 4},
    --MELEE
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/082.png"), deckIndex = 5},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/083.png"), deckIndex = 6},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/086.png"), deckIndex = 7},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/089.png"), deckIndex = 8},
    card:new{value = 4, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/092.png"), deckIndex = 9},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/093.png"), deckIndex = 10},
    card:new{value = 1, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/095.png"), deckIndex = 11},
    card:new{value = 1, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/096.png"), deckIndex = 12},
    card:new{value = 1, zone = MELEE, image = love.graphics.newImage("assets/Northern Realms/097.png"), deckIndex = 13},
    --DISTANCE
    card:new{value = 6, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/076.png"), deckIndex = 14},
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/084.png"), deckIndex = 15},
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/085.png"), deckIndex = 16},
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/087.png"), deckIndex = 17},
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/090.png"), deckIndex = 18},
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/091.png"), deckIndex = 19},
    card:new{value = 6, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/076.png"), deckIndex = 20},
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/084.png"), deckIndex = 21},
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/085.png"), deckIndex = 22},
    card:new{value = 5, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/087.png"), deckIndex = 23},
    card:new{value = 4, zone = DISTANCE, image = love.graphics.newImage("assets/Northern Realms/090.png"), deckIndex = 24},
    --SIEGE
    card:new{value = 8, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/075.png"), deckIndex = 25},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/077.png"), deckIndex = 26},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/078.png"), deckIndex = 27},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/079.png"), deckIndex = 28},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/080.png"), deckIndex = 29},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/081.png"), deckIndex = 30},
    card:new{value = 5, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/088.png"), deckIndex = 31},
    card:new{value = 1, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/094.png"), deckIndex = 32},
    card:new{value = 1, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/098.png"), deckIndex = 33},
    card:new{value = 1, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/099.png"), deckIndex = 34},
    card:new{value = 1, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/100.png"), deckIndex = 35},
    card:new{value = 8, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/075.png"), deckIndex = 36},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/077.png"), deckIndex = 37},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/078.png"), deckIndex = 38},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/079.png"), deckIndex = 39},
    card:new{value = 6, zone = SIEGE, image = love.graphics.newImage("assets/Northern Realms/080.png"), deckIndex = 40},
    --weather
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 41},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 42},
    card:new{value = 0, zone = WEATHER, power = capacity.bitingFrost, image = love.graphics.newImage("assets/Neutral/004.png"), deckIndex = 43},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 44},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 45},
    card:new{value = 0, zone = WEATHER, power = capacity.impenetrableFog, image = love.graphics.newImage("assets/Neutral/005.png"), deckIndex = 46},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 47},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 48},
    card:new{value = 0, zone = WEATHER, power = capacity.torrentialRain, image = love.graphics.newImage("assets/Neutral/006.png"), deckIndex = 49},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 50},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 51},
    card:new{value = 0, zone = WEATHER, power = capacity.clearWeather, image = love.graphics.newImage("assets/Neutral/007.png"), deckIndex = 52},

    --Special
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 53}, -- this card can go everywhere
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 54},
    card:new{value = 0, zone = BOARD, image = love.graphics.newImage("assets/Neutral/001.png"), deckIndex = 55},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 56}, -- this card can go everywhere
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 57},
    card:new{value = 0, zone = BOOST, image = love.graphics.newImage("assets/Neutral/002.png"), deckIndex = 58},
    --incineration to be implemented

    --Unique
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/008.png"), deckIndex = 59},
    card:new{value = 6, zone = MELEE, image = love.graphics.newImage("assets/Neutral/009.png"), deckIndex = 60},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/010.png"), deckIndex = 61},
    card:new{value = 2, zone = MELEE, image = love.graphics.newImage("assets/Neutral/011.png"), deckIndex = 62},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/138.png"), deckIndex = 63},
    card:new{value = 15, zone = MELEE, image = love.graphics.newImage("assets/Neutral/139.png"), deckIndex = 64},
    card:new{value = 7, zone = DISTANCE, image = love.graphics.newImage("assets/Neutral/140.png"), deckIndex = 65},
    card:new{value = 7, zone = MELEE, image = love.graphics.newImage("assets/Neutral/141.png"), deckIndex = 66},
    card:new{value = 0, zone = MELEE, image = love.graphics.newImage("assets/Neutral/142.png"), deckIndex = 67},
    card:new{value = 5, zone = MELEE, image = love.graphics.newImage("assets/Neutral/155.png"), deckIndex = 68}
  }
}
